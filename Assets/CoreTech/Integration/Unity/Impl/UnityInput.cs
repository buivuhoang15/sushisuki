using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Hoang.CoreTech.Integration.Unity.Impl
{
    public class UnityInput: IUnityInput
    {
        public event Action<GameObject, Vector3, float, bool> OnScreenTouched;
        public event Action OnTouchReleased;
        public bool IsTouchInProgress { get; private set; }

        public UnityInput([NotNull] IUnityLifecycle unityLifecycle)
        {
            Input.simulateMouseWithTouches = true;
            unityLifecycle.OnUpdate += OnUpdate;
        }

        private void OnUpdate(float deltaTime)
        {
            if (Input.touchCount > 0)
            {
                IsTouchInProgress = true;
                var touch = Input.GetTouch(0); // get first touch since touch count is greater than zero

                var isFirstTouch = touch.phase == TouchPhase.Began;
                
                if (touch.phase == TouchPhase.Stationary 
                    || touch.phase == TouchPhase.Moved
                    || isFirstTouch) 
                {
                    OnTouch(touch.position.x, touch.position.y, deltaTime, isFirstTouch);
                } 

                return;
            }
            if (Input.GetMouseButton(0))
            {
                IsTouchInProgress = true;
                var position = Input.mousePosition;
                OnTouch(position.x, position.y, deltaTime, Input.GetMouseButtonDown(0));
                
                return;
            }

            if (IsTouchInProgress)
            {
                OnTouchReleased?.Invoke();
            }
            IsTouchInProgress = false;
        }
        
        private void OnTouch(float x, float y, float deltaTime, bool isFirstTouch)
        {
            if (Camera.main != null)
            {
                // Get the touch position from the screen touch to world point   
                var touchedPos = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 10));
                // For some reason that 10 is very necessary

                RaycastHit2D hitInformation = Physics2D.Raycast(touchedPos, Camera.main.transform.forward);

                GameObject hitObject = null;
                if (hitInformation.collider != null) {
                    // We should have hit something with a 2D Physics collider
                    hitObject = hitInformation.transform.gameObject;
                }
                
                OnScreenTouched?.Invoke(hitObject, touchedPos, deltaTime, isFirstTouch);
            }
        }
    }
}