using Hoang.CoreTech.Infrastructure.Logging;

namespace Hoang.CoreTech.Integration.Logging
{
    internal class ConsoleLogTarget : ILogTarget
    {
        public void Debug(string message)
        {
            UnityEngine.Debug.Log(message);
        }

        public void Info(string message)
        {
            Debug(message);
        }

        public void Warn(string message)
        {
            UnityEngine.Debug.LogWarning(message);
        }

        public void Error(string message)
        {
            UnityEngine.Debug.LogError(message);
        }
    }
}