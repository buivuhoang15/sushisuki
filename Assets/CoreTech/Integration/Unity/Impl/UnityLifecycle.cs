using System;
using System.Collections;
using UnityEngine;

namespace Hoang.CoreTech.Integration.Unity.Impl
{
    internal class UnityLifecycle : MonoBehaviour, IUnityLifecycle
    {
        public event Action<float> OnUpdate;
        public event Action OnAwake;
        public event Action OnStart;
        public event Action OnLateUpdate;
        public event Action OnQuit;

        public void StartUnityCoroutine(IEnumerator doWork)
        {
            StartCoroutine(doWork);
        }

        protected void Update()
        {
            OnUpdate?.Invoke(Time.deltaTime);
        }

        protected void LateUpdate()
        {
            OnLateUpdate?.Invoke();
        }

        protected void Start()
        {
            OnStart?.Invoke();
        }

        protected void Awake()
        {
            DontDestroyOnLoad(this);
            OnAwake?.Invoke();
        }

        protected void OnApplicationQuit()
        {
            OnQuit?.Invoke();
        }
    }
}