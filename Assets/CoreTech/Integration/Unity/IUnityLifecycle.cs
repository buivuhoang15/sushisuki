using System;
using System.Collections;

namespace Hoang.CoreTech.Integration.Unity
{
    public interface IUnityLifecycle
    {
        event Action<float> OnUpdate;

        event Action OnAwake;

        event Action OnStart;

        event Action OnLateUpdate;

        event Action OnQuit;

        void StartUnityCoroutine(IEnumerator doWork);
    }
}