namespace Scenes.GameStart
{
    public interface IRandomFoodGenerator
    {
        IFood Next();
    }
}