﻿using Random = UnityEngine.Random;

namespace Scenes.MainMenu
{
    public class Food : MovableItem
    {
        protected override void OnStart()
        {
            State = ItemState.FreeFloating;

            SetPositionForWigglingInPlace();

            var rand = Random.Range(0, 0.1f);
            transform.Translate(0, rand, 0);
        }
    }
}