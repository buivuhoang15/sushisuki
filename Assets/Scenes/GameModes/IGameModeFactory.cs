namespace Scenes.GameModes
{
    public interface IGameModeFactory
    {
        IGameMode Create(GameMode mode);
    }
}