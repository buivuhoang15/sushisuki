using System;

namespace Hoang.CoreTech.Infrastructure.Async
{
    public interface IDeferredCall : IDisposable
    {
        void Start();

        void Stop();

        void Restart();

        /// <summary>
        /// Occurs when call is either finished or stopped
        /// </summary>
        event Action OnQuit;
    }
}