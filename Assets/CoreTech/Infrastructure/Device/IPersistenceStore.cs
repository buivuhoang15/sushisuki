namespace Hoang.CoreTech.Infrastructure.Device
{
    public interface IPersistenceStore
    {
        bool Add(string key, object value);
        string GetString(string key);
        bool Commit();
        bool ContainsKey(string key);
    }
}