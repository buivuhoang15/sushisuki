using System;
using System.Collections.Generic;
using System.Linq;

namespace Hoang.CoreTech.Infrastructure.Async
{
    public class DeferredCallFactory : IDeferredCallFactory
    {
        private readonly ISet<IDeferredCall> _handles = new HashSet<IDeferredCall>();

        public IDeferredCall Execute(TimeSpan delay, Action callback)
        {
            var deferredCall = new AsyncAwaitDeferredCall(delay, callback);
            _handles.Add(deferredCall);
            deferredCall.OnQuit += () => { _handles.Remove(deferredCall); };
            deferredCall.Start();
            return deferredCall;
        }

        public void Dispose()
        {
            foreach (var deferredCall in _handles.ToList())
            {
                deferredCall.Dispose();
            }
        }
    }
}