﻿using Hoang.CoreTech.Infrastructure.Async;
using Hoang.CoreTech.Infrastructure.Device;
using Hoang.CoreTech.Infrastructure.Logging;
using Hoang.CoreTech.Integration.GameSession;
using Hoang.CoreTech.Integration.Scoring;
using Hoang.CoreTech.Integration.Unity;

namespace Hoang.CoreTech
{
    public interface ICoreTechContext
    {
        IUnityLifecycle UnityLifecycle { get; }
        
        IUnityInput UnityInput { get; }

        IDeferredCallFactory DeferredCallFactory { get; }

        ICoreTechLogger Logger { get; }
        
        ILeaderBoardWriter LeaderBoard { get; }
        
        IGameSessionWriter GameSession { get; }
        
        IPersistenceStore Store { get; }
    }
}