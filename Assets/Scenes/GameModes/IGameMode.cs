using System;
using Scenes.GameStart;

namespace Scenes.GameModes
{
    public interface IGameMode: IDisposable
    {
        IFood EdibleFood { get; }
        
        bool IsTimed { get; }

        TimeSpan RemainingTime { get; }

        float SpeedMultiplier { get; }

        /// <summary>
        /// Notifies the game mode that a food has escaped the screen
        /// </summary>
        /// <param name="food">The food object</param>
        void FoodEscaped(IFood food);

        /// <summary>
        /// Notifies the game mode that a food has been eaten
        /// </summary>
        /// <param name="food">The food object</param>
        void FoodEaten(IFood food);

        /// <summary>
        /// Occurs when the edible food is changed
        /// </summary>
        event Action<IFood> OnEdibleFoodChanged;
    }
    
    public enum GameMode
    {
        FullCourse, // Don't let food go out of screen. Speed increases as number of food gets eaten
        QuickMeal, // Like full course, but player only has 30 seconds. Speed increases every 5 seconds 
        PickyEater, // Like full course, but only certain types of food can be eaten. Loses if wrong food is eaten
        Relax // No time limit and cannot lose
    }

    public static class GameModeExtensions
    {
        public static Tuple<string, string> GetDescription(this GameMode mode)
        {
            switch (mode)
            {
                case GameMode.FullCourse:
                    return Tuple.Create(
                        "Full Course",
                        "Don't let food go out of screen. Speed increases as number of food gets eaten");
                case GameMode.QuickMeal:
                    return Tuple.Create(
                        "Quick Meal",
                        "Like full course, but you only has 30 seconds. Speed increases every 5 seconds");
                case GameMode.PickyEater:
                    return Tuple.Create(
                        "Picky Eater",
                        "Like full course, but only certain types of food can be eaten. Loses if wrong food is eaten");
                case GameMode.Relax:
                    return Tuple.Create(
                        "Relax",
                        "Take your time and enjoy the food :). Until you are bored");
                default:
                    throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
            }
        }
    }
}