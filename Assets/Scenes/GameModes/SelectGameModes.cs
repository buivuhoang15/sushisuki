using System;
using Hoang.CoreTech.Infrastructure.Device;
using Hoang.CoreTech.Integration.GameSession;
using UnityEngine;
using UnityEngine.UI;

namespace Scenes.GameModes
{
    public class SelectGameModes : SceneBootstrapper
    {
        private IPersistenceStore Store => Context.Store;
        private IGameSessionWriter GameSession => Context.GameSession;
        
        public Transform BtnPrefab;
        public Transform Canvas;
        
        protected override void OnStart()
        {
            base.OnStart();
            var gameModes = Enum.GetValues(typeof(GameMode));

            var nextTranslateTop = 0f;
            foreach (var gameMode in gameModes)
            {
                var properties = ((GameMode) gameMode).GetDescription();
                var gameName = properties.Item1;
                var description = properties.Item2;
                
                var button = Instantiate(BtnPrefab, Canvas, false);
                button.Translate(0, nextTranslateTop, 0);

                nextTranslateTop -= 1.5f;

                var buttonScript = button.GetComponent<Button>();
                buttonScript.onClick.AddListener(() =>
                {
                    OnSelectMode((GameMode) gameMode);
                });
                
                var nameText = button.GetChild(1);
                nameText.GetComponent<Text>().text = gameName;
                
                var descriptionText = button.GetChild(2);
                descriptionText.GetComponent<Text>().text = description;
            }
        }

        private void OnSelectMode(GameMode gameMode)
        {
            Log.Debug(this, "{0} game mode selected", gameMode);
            Store.Add(Consts.GameModeKey, gameMode);
            GameSession.StartGame();
        }
    }
}