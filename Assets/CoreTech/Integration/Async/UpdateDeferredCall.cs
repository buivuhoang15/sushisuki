using System;
using Hoang.CoreTech.Infrastructure.Async;
using Hoang.CoreTech.Integration.Unity;
using UnityEngine;

namespace Hoang.CoreTech.Integration.Async
{
    /// <summary>
    /// This uses Unity update loop to schedule tasks.
    /// Tasks will not be executed if long running code is blocking the update loop. 
    /// </summary>
    public class UpdateDeferredCall : IDeferredCall
    {
        private readonly IUnityLifecycle _unity;
        private readonly float _delay;
        private float _passedTime;
        private readonly Action _callback;
        private bool _stopped;

        public UpdateDeferredCall(IUnityLifecycle unity, TimeSpan delay, Action callback)
        {
            _unity = unity;
            _delay = (float) delay.TotalSeconds;
            _callback = callback;
        }

        public void Start()
        {
            _unity.OnUpdate += DoWork;
        }

        private void DoWork(float deltaTime)
        {
            if (_delay > 0 && _passedTime < _delay && !_stopped)
            {
                _passedTime += deltaTime;
                return;
            }

            if (_stopped)
            {
                _unity.OnUpdate -= DoWork;
                OnQuit?.Invoke();
                return;
            }

            _callback();
            _unity.OnUpdate -= DoWork;
            OnQuit?.Invoke();
        }

        public void Stop()
        {
            _stopped = true;
        }

        public void Restart()
        {
            Stop();
            Start();
        }

        public event Action OnQuit;

        public void Dispose()
        {
            Stop();
        }
    }
}