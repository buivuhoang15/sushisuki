﻿using System;
using Hoang.CoreTech.Infrastructure.Logging;
using Hoang.CoreTech.Integration.Unity.Impl;
using UnityEngine;

namespace Hoang.CoreTech
{
    public abstract class CoreTechMonoBehaviour : MonoBehaviour
    {
        private static ICoreTechContext _contextInstance;
        private bool _isBeingDragged;
        protected ICoreTechContext Context => _contextInstance;
        protected ICoreTechLogger Log => Context?.Logger;
        protected event Action<bool> OnVisibilityChanged;

        private bool IsBeingDragged
        {
            get { return _isBeingDragged; }
            set
            {
                if (_isBeingDragged && !value)
                {
                    OnTouchReleased();
                }
                _isBeingDragged = value;
            }
        }

        // Use this for initialization
        protected void Start()
        {
            if (_contextInstance == null)
            {
                var bootstrap = new GameObject("CoreTech");
                var unityLifecycle = bootstrap.AddComponent<UnityLifecycle>();
                _contextInstance = new CoreTechContext(unityLifecycle);

                // Any other 1 time initialisation goes here
                Application.targetFrameRate = 60;
                Input.simulateMouseWithTouches = true;
                Screen.orientation = ScreenOrientation.Portrait;
            }
            
            // Any initialisation that needs to be done for each GameObject goes here
            Context.UnityInput.OnScreenTouched += OnScreenTouched;
            Context.UnityInput.OnTouchReleased += OnScreenTouchReleased;

            OnStart();
        }

        private void OnScreenTouched(GameObject gObject, Vector3 touchedPos, float delta, bool isFirstTouch)
        {
            if (gameObject == null)
            {
                return;
            }

            if (gameObject == gObject && isFirstTouch 
                || IsBeingDragged && Context.UnityInput.IsTouchInProgress)
            {
                IsBeingDragged = true;
                OnTouched(touchedPos, delta);
            }
            else
            {
                IsBeingDragged = false;
            }
        }
        
        private void OnScreenTouchReleased()
        {
            IsBeingDragged = false;
        }

        protected void Awake()
        {
            OnAwake();
        }

        protected void Update()
        {
            OnUpdate(Time.deltaTime);
        }

        protected virtual void OnStart()
        {
        }

        protected virtual void OnUpdate(float deltaTime)
        {
        }

        protected virtual void OnAwake()
        {
        }

        /// <summary>
        /// This is called whenever this <see cref="GameObject"/> is touched
        /// </summary>
        /// <param name="touchedPos">The position that was touched</param>
        /// <param name="deltaTime">The delta time</param>
        protected virtual void OnTouched(Vector3 touchedPos, float deltaTime)
        {
        }

        protected virtual void OnTouchReleased()
        {
        }

        protected virtual void OnDestroyed()
        {
        }
        
        protected void OnBecameVisible()
        {
            OnVisibilityChanged?.Invoke(true);
        }

        protected void OnBecameInvisible()
        {
            OnVisibilityChanged?.Invoke(false);
        }

        private void OnDestroy()
        {
            Context.UnityInput.OnScreenTouched -= OnScreenTouched;
            Context.UnityInput.OnTouchReleased -= OnScreenTouchReleased;
            OnDestroyed();
        }
    }
}