using System;
using Hoang.CoreTech.Infrastructure.Async;
using Hoang.CoreTech.Infrastructure.Device;
using Hoang.CoreTech.Infrastructure.Device.Impl;
using Hoang.CoreTech.Infrastructure.Logging;
using Hoang.CoreTech.Infrastructure.Logging.Impl;
using Hoang.CoreTech.Integration.Async;
using Hoang.CoreTech.Integration.GameSession;
using Hoang.CoreTech.Integration.GameSession.Impl;
using Hoang.CoreTech.Integration.Logging;
using Hoang.CoreTech.Integration.Scoring;
using Hoang.CoreTech.Integration.Scoring.Impl;
using Hoang.CoreTech.Integration.Unity;
using Hoang.CoreTech.Integration.Unity.Impl;

namespace Hoang.CoreTech
{
    public class CoreTechContext : ICoreTechContext, IDisposable
    {
        public CoreTechContext(IUnityLifecycle unityLifecycle)
        {
            DeferredCallFactory = new UnityDeferredCallFactory(unityLifecycle);
            //DeferredCallFactory = new DeferredCallFactory();
            Logger = new CoreTechLogger();
#if DEBUG
            Logger.AddLogTarget(new ConsoleLogTarget());
#endif

            UnityInput = new UnityInput(unityLifecycle);

            GameSession = new GameSession();

            LeaderBoard = new LeaderBoard(GameSession);
            LeaderBoard.Initialise();

            Store = new PersistenceStore();

            UnityLifecycle = unityLifecycle;
            UnityLifecycle.OnQuit += Dispose;
        }

        public IUnityLifecycle UnityLifecycle { get; }
        
        public IUnityInput UnityInput { get; }

        public IDeferredCallFactory DeferredCallFactory { get; }

        public ICoreTechLogger Logger { get; }
        
        public ILeaderBoardWriter LeaderBoard { get; }
        
        public IGameSessionWriter GameSession { get; }
        
        public IPersistenceStore Store { get; }

        public void Dispose()
        {
            DeferredCallFactory.Dispose();
        }
    }
}