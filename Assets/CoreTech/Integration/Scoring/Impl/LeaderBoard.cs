using System;
using Hoang.CoreTech.Integration.GameSession;

namespace Hoang.CoreTech.Integration.Scoring.Impl
{
    internal class LeaderBoard : ILeaderBoardWriter
    {
        public int HighScore { get; private set; }
        public int CurrentScore { get; private set; }

        public LeaderBoard(IGameSessionReader gameSession)
        {
            gameSession.OnStartGame += () => { CurrentScore = 0; };
        }

        public void Initialise()
        {
            HighScore = 10;
            LeaderBoardUpdated?.Invoke();
        }

        public void IncreaseCurrentScore(int amount)
        {
            CurrentScore += amount;
            CurrentScoreUpdated?.Invoke();
        }

        public void CommitScores()
        {
            if (CurrentScore > HighScore)
            {
                HighScore = CurrentScore;
                LeaderBoardUpdated?.Invoke();
            }
        }

        public event Action CurrentScoreUpdated;
        public event Action LeaderBoardUpdated;
    }
}