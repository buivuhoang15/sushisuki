using System;
using Hoang.CoreTech.Infrastructure.Logging;
using Hoang.CoreTech.Integration.GameSession;
using Hoang.CoreTech.Integration.Scoring;
using Scenes.GameStart;

namespace Scenes.GameModes.Modes
{
    public class Relax: IGameMode
    {
        private readonly IGameSessionWriter _gameSession;
        private readonly ILeaderBoardWriter _leaderBoard;
        private readonly ICoreTechLogger _logger;
        private int _previousScoreLevel;

        public IFood EdibleFood => null;
        public bool IsTimed => false;
        public TimeSpan RemainingTime => TimeSpan.Zero;
        public float SpeedMultiplier { get; private set; } = 1;

        public Relax(IGameSessionWriter gameSession, ILeaderBoardWriter leaderBoard, ICoreTechLogger logger)
        {
            _gameSession = gameSession;
            _leaderBoard = leaderBoard;
            _logger = logger;
            _leaderBoard.CurrentScoreUpdated += OnScoreUpdated;
        }

        private void OnScoreUpdated()
        {
            int scoreLevel = _leaderBoard.CurrentScore / 5;
            SpeedMultiplier = 1 + scoreLevel * 0.5f ;

            if (_previousScoreLevel == scoreLevel)
            {
                return;
            }

            _previousScoreLevel = scoreLevel;
            _logger.Debug(this, "SpeedMultiplier: {0}", SpeedMultiplier);
        }

        public void FoodEscaped(IFood food)
        {
        }

        public void FoodEaten(IFood food)
        {
            _leaderBoard.IncreaseCurrentScore(1);
        }

        public event Action<IFood> OnEdibleFoodChanged;
        
        public void Dispose()
        {
            _leaderBoard.CurrentScoreUpdated -= OnScoreUpdated;
        }
    }
}