﻿using System;
using System.Threading;
using Hoang.CoreTech.Infrastructure.Async;
using NUnit.Framework;
using UnityEngine;

namespace Hoang.Tests.CoreTech.Infrastructure.Async
{
    public class DeferredCallTests
    {
        private IDeferredCall _deferredCall;
        private bool _wasCalled;
        private ManualResetEventSlim _finishEvent;

        [SetUp]
        public void SetUp()
        {
            _wasCalled = false;
            _finishEvent = new ManualResetEventSlim();
            _deferredCall = new AsyncAwaitDeferredCall(TimeSpan.FromMilliseconds(10), () =>
            {
                Debug.Log("This was definitely called");
                _wasCalled = true;
                _finishEvent.Set();
            });
            _deferredCall.Start();
        }

        [TearDown]
        public void TearDown()
        {
            _finishEvent.Dispose();
            _deferredCall.Dispose();
        }

        [Test]
        public void ShouldExecuteCallback_AfterDelay()
        {
            Assert.IsFalse(_wasCalled);
            _finishEvent.Wait(TimeSpan.FromMilliseconds(200));
            Debug.Log("This should be called after");
            Assert.IsTrue(_wasCalled);
        }

        [Test]
        public void ShouldNotExecuteCallback_IfStopped()
        {
            _deferredCall.Start();

            _deferredCall.Stop();
            _finishEvent.Wait(TimeSpan.FromMilliseconds(20));
            Assert.IsFalse(_wasCalled);
        }

        [Test]
        public void ShouldNotExecute_IfDisposed()
        {
            _deferredCall.Start();

            _deferredCall.Dispose();
            _finishEvent.Wait(TimeSpan.FromMilliseconds(20));
            Assert.IsFalse(_wasCalled);
        }
    }
}