using System.Collections.Generic;

namespace Hoang.CoreTech.Infrastructure.Device.Impl
{
    internal class PersistenceStore : IPersistenceStore
    {
        private readonly IDictionary<string, string> _data = new Dictionary<string, string>();
        
        public bool Add(string key, object value) 
        {
            _data[key] = value.ToString();
            return true;
        }

        public string GetString(string key)
        {
            return _data[key];
        }

        public bool Commit()
        {
            return true;
        }

        public bool ContainsKey(string key)
        {
            return _data.ContainsKey(key);
        }
    }
}