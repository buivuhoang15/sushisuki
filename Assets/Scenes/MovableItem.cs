using Hoang.CoreTech;
using UnityEngine;

namespace Scenes
{
    public class MovableItem: CoreTechMonoBehaviour
    {
        public const float WiggleSpeed = 0.5f;
        
        protected enum ItemState
        {
            InConveyor,
            FreeFloating,
            InsideTarget
        }

        protected ItemState State;

        private bool _isMovingDown;
        private float _maxY;
        private float _minY;
        private float CurrentY => transform.position.y;

        protected void SetPositionForWigglingInPlace()
        {
            _minY = CurrentY;
            _maxY = _minY + 0.1f;
        }
        
        protected override void OnUpdate(float deltaTime)
        {
            if (State == ItemState.FreeFloating)
            {
                if (CurrentY < _maxY && !_isMovingDown)
                {
                    MoveUp(deltaTime);
                }
                else if (CurrentY > _minY)
                {
                    _isMovingDown = true;
                    MoveDown(deltaTime);
                }
                else
                {
                    _isMovingDown = false;
                }
            }
        }
        
        protected override void OnTouched(Vector3 touchedPos, float deltaTime)
        {
            transform.position = Vector3.Lerp(transform.position, touchedPos, deltaTime * 10);
        }

        protected override void OnTouchReleased()
        {
            if (State == ItemState.FreeFloating)
            {
                SetPositionForWigglingInPlace();
            }
        }

        private void MoveUp(float deltaTime)
        {
            transform.Translate(0, WiggleSpeed * deltaTime, 0);
        }

        private void MoveDown(float deltaTime)
        {
            transform.Translate(0, -WiggleSpeed * deltaTime, 0);
        }
    }
}