using System;
using System.Collections.Generic;
using System.Linq;
using Hoang.CoreTech.Infrastructure.Async;
using Hoang.CoreTech.Integration.Unity;

namespace Hoang.CoreTech.Integration.Async
{
    public class UnityDeferredCallFactory : IDeferredCallFactory
    {
        private readonly IUnityLifecycle _unity;
        private readonly ISet<IDeferredCall> _handles = new HashSet<IDeferredCall>();

        public UnityDeferredCallFactory(IUnityLifecycle unity)
        {
            _unity = unity;
        }

        public IDeferredCall Execute(TimeSpan delay, Action callback)
        {
            var deferredCall = new CoroutineDeferredCall(_unity, delay, callback);
            _handles.Add(deferredCall);
            deferredCall.OnQuit += () => { _handles.Remove(deferredCall); };
            deferredCall.Start();
            return deferredCall;
        }

        public void Dispose()
        {
            foreach (var deferredCall in _handles.ToList())
            {
                deferredCall.Dispose();
            }
        }
    }
}