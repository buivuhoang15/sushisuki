using System;

namespace Hoang.CoreTech.Integration.GameSession.Impl
{
    internal class GameSession : IGameSessionWriter
    {
        public GameState State
        {
            get;
#if DEBUG
            set;
#else
            private set;
#endif
        }
        public event Action OnStartGame;
        public event Action OnSelectGameMode;
        public event Action OnGameOver;

        public void StartGame()
        {
            if (!ChangeState(GameState.GameStart))
            {
                return;
            }

            OnStartGame?.Invoke();
        }

        public void GameOver()
        {
            if (!ChangeState(GameState.GameOver))
            {
                return;
            }

            OnGameOver?.Invoke();
        }

        public void SelectGameMode()
        {
            if (!ChangeState(GameState.SelectGameMode))
            {
                return;
            }
            
            OnSelectGameMode?.Invoke();
        }

        private bool ChangeState(GameState newState)
        {
            if (State == newState)
            {
                return false;
            }

            State = newState;
            return true;
        }
    }
}