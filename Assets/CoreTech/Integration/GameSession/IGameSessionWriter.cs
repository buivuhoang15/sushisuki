namespace Hoang.CoreTech.Integration.GameSession
{
    public interface IGameSessionWriter : IGameSessionReader
    {
        void StartGame();

        void GameOver();

        void SelectGameMode();
    }
}