﻿using Scenes.GameModes;
using UnityEngine;

namespace Scenes.GameStart
{
    public class Food : MovableItem, IFood
    {
        private const float ConveyorSpeed = -1f;
        private bool _appearedOnScreen;

        private bool _droppedOnTarget;

        protected override void OnStart()
        {
            State = ItemState.InConveyor;
            OnVisibilityChanged += visibility =>
            {
                if (_droppedOnTarget)
                {
                    return;
                }
                if (visibility)
                {
                    _appearedOnScreen = true;
                    return;
                }

                if (!_appearedOnScreen)
                {
                    return;
                }

                GameMode.FoodEscaped(this);
                Destroy(gameObject);
            };
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (IsCollidingWithConveyor(other))
            {
                State = ItemState.InConveyor;
            }
            else if (IsCollidingWithTarget(other))
            {
                // Brought food to the target
                State = ItemState.InsideTarget;
            }
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            if (IsCollidingWithConveyor(other) || IsCollidingWithTarget(other))
            {
                State = ItemState.FreeFloating;
            }
        }

        private bool IsCollidingWithConveyor(Collision2D other)
        {
            return other.gameObject.name == "Conveyor";
        }

        private bool IsCollidingWithTarget(Collision2D other)
        {
            return other.gameObject.CompareTag("Target");
        }

        protected override void OnTouchReleased()
        {
            base.OnTouchReleased();
            if (State == ItemState.InsideTarget)
            {
                _droppedOnTarget = true;
                GameMode.FoodEaten(this);
                Destroy(gameObject);
            }
        }

        protected override void OnUpdate(float deltaTime)
        {
            switch (State)
            {
                case ItemState.InConveyor:
                    transform.Translate(0, ConveyorSpeed 
                                           * GameMode.SpeedMultiplier
                                           * deltaTime, 0);
                    break;
                default:
                    base.OnUpdate(deltaTime);
                    break;
            }
        }

        // Injected dependencies due to being MonoBehaviours
        public int Type { get; set; }

        public IGameMode GameMode { get; set; }
    }
}