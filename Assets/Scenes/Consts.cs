namespace Scenes
{
    public static class Consts
    {
        // Resources
        public const string FoodSprite = "Sprites/Food";

        // Scenes
        public const string GameStartScene = "GameStart";
        public const string MainMenuScene = "MainMenu";
        public const string SelectGameModesScene = "SelectGameModes";

        // Other values
        public const string GameModeKey = "game_mode";
    }
}