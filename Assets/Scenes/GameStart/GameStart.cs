﻿using System;
using System.Globalization;
using System.Linq;
using Hoang.CoreTech.Infrastructure.Async;
using Hoang.CoreTech.Infrastructure.Device;
using Hoang.CoreTech.Integration.GameSession;
using Hoang.CoreTech.Integration.Scoring;
using Scenes.GameModes;
using UnityEngine;
using UnityEngine.UI;

namespace Scenes.GameStart
{
    public class GameStart : SceneBootstrapper
    {
        public Text ScoreText;
        public Text TimeLeftText;
        public Transform FoodPrefab;
        public Transform Canvas;
        public Conveyor Conveyor;
        public GameObject EdibleFoodPreview;
        public GameMode GameMode;

        private IPersistenceStore Store => Context.Store;
        private ILeaderBoardWriter LeaderBoard => Context.LeaderBoard;
        private IDeferredCallFactory DeferredCallFactory => Context.DeferredCallFactory;

        private float FoodRespawnTime => 1;
        private Sprite[] _foodSprites;
        private IRandomFoodGenerator _randomFoodGenerator;
        private IGameModeFactory _gameModeFactory;
        private IGameMode _gameMode;

        protected override void OnStart()
        {
            base.OnStart();
#if DEBUG
            Context.GameSession.State = GameState.GameStart;
#endif

            if (Store.ContainsKey(Consts.GameModeKey))
            {
                Enum.TryParse(Store.GetString(Consts.GameModeKey), out GameMode);
            }

            Log.Debug(this, "Starting game mode {0}", GameMode);
            
            _foodSprites = Resources.LoadAll<Sprite>(Consts.FoodSprite);

            if (!_foodSprites.Any())
            {
                Log.Error(this, "No food sprite found!");
                return;
            }
            
            _randomFoodGenerator = new RandomFoodGenerator(_foodSprites.Length);

            SetUpGameMode();

            LeaderBoard.CurrentScoreUpdated += OnScoreUpdated;
            DeferredCallFactory.Execute(TimeSpan.FromSeconds(FoodRespawnTime), CreateFood);
        }

        private void SetUpGameMode()
        {
            _gameModeFactory = new GameModeFactory(Context.UnityLifecycle,
                Context.GameSession,
                LeaderBoard,
                Log,
                _randomFoodGenerator);
            _gameMode = _gameModeFactory.Create(GameMode);
            Conveyor.GameMode = _gameMode;
            if (_gameMode.EdibleFood != null)
            {
                EdibleFoodPreview.SetActive(true);
                EdibleFoodPreview.GetComponent<SpriteRenderer>().sprite = _foodSprites[_gameMode.EdibleFood.Type];
                _gameMode.OnEdibleFoodChanged += OnEdibleFoodChanged;
            }
            SetTimeLeftText();
        }

        private void OnEdibleFoodChanged(IFood edibleFood)
        {
            EdibleFoodPreview.GetComponent<SpriteRenderer>().sprite = _foodSprites[edibleFood.Type];
        }

        private void SetTimeLeftText()
        {
            TimeLeftText.text = _gameMode.IsTimed
                ? ((int)_gameMode.RemainingTime.TotalSeconds).ToString(CultureInfo.InvariantCulture)
                : "Infinite";
        }

        private void OnScoreUpdated()
        {
            ScoreText.text = LeaderBoard.CurrentScore.ToString();
        }

        private void CreateFood()
        {
            var food = Instantiate(FoodPrefab);
            food.SetParent(Canvas, false);

            var randomFood = _randomFoodGenerator.Next();
            food.GetComponent<SpriteRenderer>().sprite = _foodSprites[randomFood.Type];
            var script = food.GetComponent<Food>();
            script.Type = randomFood.Type;
            script.GameMode = _gameMode;

            food.gameObject.SetActive(true);

            DeferredCallFactory.Execute(TimeSpan.FromSeconds(FoodRespawnTime), CreateFood);
        }

        protected override void OnUpdate(float deltaTime)
        {
            SetTimeLeftText();
        }

        protected override void OnDestroyed()
        {
            LeaderBoard.CurrentScoreUpdated -= OnScoreUpdated;
            LeaderBoard.CommitScores();
            DeferredCallFactory.Dispose();
            _gameMode.OnEdibleFoodChanged -= OnEdibleFoodChanged;
            _gameMode.Dispose();
        }
    }
}