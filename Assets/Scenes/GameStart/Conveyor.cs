﻿using Hoang.CoreTech;
using Scenes.GameModes;

namespace Scenes.GameStart
{
    public class Conveyor : CoreTechMonoBehaviour
    {
        private bool _appearedOnScreen;
        private const float Speed = -1f;

        private float _originalY;

        protected override void OnStart()
        {
            _originalY = transform.position.y;
        }

        private void MoveToTop()
        {
            transform.Translate(0, _originalY - transform.position.y, 0);
        }

        protected override void OnUpdate(float deltaTime)
        {
            transform.Translate(0, Speed 
                                   * GameMode.SpeedMultiplier
                                   * deltaTime, 0);

            if (transform.position.y <= 0)
            {
                MoveToTop();
            }
        }
        
        // Injected dependencies due to being MonoBehaviours
        public IGameMode GameMode { get; set; }
    }
}