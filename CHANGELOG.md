# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/buivuhoang15/sushisuki/compare/v0.0.3...v0.1.0) (2018-11-14)


### Features

* MVP reached with game session and scene mapper ([e6020de](https://gitlab.com/buivuhoang15/sushisuki/commit/e6020de))



<a name="0.0.3"></a>
## [0.0.3](https://gitlab.com/buivuhoang15/sushisuki/compare/v0.0.2...v0.0.3) (2018-11-14)


### Bug Fixes

* DeferredCallFactory clean up issue, UnityLifeCycle being destroyed ([26d9a94](https://gitlab.com/buivuhoang15/sushisuki/commit/26d9a94))
* stop disposing task in AwaitAsyncDeferredCall ([3144b19](https://gitlab.com/buivuhoang15/sushisuki/commit/3144b19))


### Features

* add in game with food and conveyor moving ([a2db759](https://gitlab.com/buivuhoang15/sushisuki/commit/a2db759))
* add working main menu with start button ([5372b48](https://gitlab.com/buivuhoang15/sushisuki/commit/5372b48))
* implement touch drag and drop for food ([a8aa870](https://gitlab.com/buivuhoang15/sushisuki/commit/a8aa870))



<a name="0.0.2"></a>
## [0.0.2](https://gitlab.com/buivuhoang15/sushisuki/compare/v0.0.1...v0.0.2) (2018-11-10)



<a name="0.0.1"></a>
## 0.0.1 (2018-11-10)
