using System;
using UnityEngine;

namespace Hoang.CoreTech.Integration.Unity
{
    public interface IUnityInput
    {
        /// <summary>
        /// Occurs when a touch is registered against a <see cref="GameObject"/>
        /// The float argument is deltaTime
        /// </summary>
        event Action<GameObject, Vector3, float, bool> OnScreenTouched;

        /// <summary>
        /// Occurs when the user stops using the touch screen
        /// </summary>
        event Action OnTouchReleased;

        /// <summary>
        /// Whether user is using the touch screen or not
        /// </summary>
        bool IsTouchInProgress { get; }
    }
}