namespace Hoang.CoreTech.Infrastructure.Logging
{
    public interface ILogTarget
    {
        void Debug(string message);

        void Info(string message);

        void Warn(string message);

        void Error(string message);
    }
}