namespace Scenes.GameStart
{
    public interface IFood
    {
        int Type { get; set; }
    }

    public static class FoodExtensions
    {
        public static bool TheSameAs(this IFood food, IFood anotherFood)
        {
            return anotherFood != null && food.Type == anotherFood.Type;
        }
    }
}