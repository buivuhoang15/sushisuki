namespace Hoang.CoreTech.Infrastructure.Logging
{
    public interface ICoreTechLogger
    {
        void AddLogTarget(ILogTarget target);

        void Debug(object source, string message, params object[] optionalParams);

        void Info(object source, string message, params object[] optionalParams);

        void Warn(object source, string message, params object[] optionalParams);

        void Error(object source, string message, params object[] optionalParams);
        
        bool IsDebug { get; }
    }
}