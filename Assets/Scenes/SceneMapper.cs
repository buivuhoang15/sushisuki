using Hoang.CoreTech;
using Hoang.CoreTech.Integration.GameSession;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scenes
{
    public class SceneMapper: CoreTechMonoBehaviour
    {
        private static SceneMapper _instance;

        private IGameSessionWriter GameSession => Context.GameSession;

        public static void Initialise()
        {
            if (_instance == null)
            {
                var bootstrap = new GameObject("SceneMapper");
                _instance = bootstrap.AddComponent<SceneMapper>();
            }
        }
        
        protected override void OnStart()
        {
            GameSession.OnGameOver += OnGameOver;
            GameSession.OnStartGame += OnStartGame;
            GameSession.OnSelectGameMode += OnSelectGameMode;
        }

        private void OnSelectGameMode()
        {
            Log.Debug(this, "Select game mode");
            SceneManager.LoadScene(Consts.SelectGameModesScene, LoadSceneMode.Single);
        }

        private void OnStartGame()
        {
            Log.Debug(this, "Game Start");
            SceneManager.LoadScene(Consts.GameStartScene, LoadSceneMode.Single);
        }

        private void OnGameOver()
        {
            Log.Debug(this, "Game Over");
            SceneManager.LoadScene(Consts.MainMenuScene, LoadSceneMode.Single);
        }

        protected override void OnDestroyed()
        {
            GameSession.OnGameOver -= OnGameOver;
            GameSession.OnStartGame -= OnStartGame;
            GameSession.OnSelectGameMode -= OnSelectGameMode;
        }

        protected override void OnAwake()
        {
            DontDestroyOnLoad(this);
        }
    }
}