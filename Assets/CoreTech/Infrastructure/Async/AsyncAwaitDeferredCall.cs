using System;
using System.Threading;
using System.Threading.Tasks;

namespace Hoang.CoreTech.Infrastructure.Async
{
    public class AsyncAwaitDeferredCall : IDeferredCall
    {
        private readonly TimeSpan _delay;
        private readonly Action _callback;
        private CancellationTokenSource _cancellationTokenSource;

        public event Action OnQuit;

        public AsyncAwaitDeferredCall(TimeSpan delay, Action callback)
        {
            _delay = delay;
            _callback = callback;
        }

        public void Start()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            DoWork(_cancellationTokenSource.Token);
        }

        private async Task DoWork(CancellationToken token)
        {
            if (_delay != TimeSpan.Zero)
            {
                var task = Task.Delay(_delay, token);

                try
                {
                    await task;
                }
                catch (TaskCanceledException)
                {
                    OnQuit?.Invoke();
                    return;
                }
            }

            _callback();
            OnQuit?.Invoke();
        }

        public void Stop()
        {
            _cancellationTokenSource?.Cancel();
        }

        public void Restart()
        {
            Stop();
            Start();
        }

        public void Dispose()
        {
            Stop();
            OnQuit?.Invoke();
        }
    }
}