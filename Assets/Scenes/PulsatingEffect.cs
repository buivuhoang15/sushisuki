using Hoang.CoreTech;
using UnityEngine;

namespace Scenes
{
    public class PulsatingEffect : CoreTechMonoBehaviour
    {
        private float _originalScaleX;
        private float _originalScaleY;

        protected override void OnStart()
        {
            _originalScaleX = transform.localScale.x;
            _originalScaleY = transform.localScale.y;
        }

        protected override void OnUpdate(float deltaTime)
        {
            var vec = new Vector3(_originalScaleX + Mathf.Sin(Time.time * 4) * 10,
                _originalScaleY + Mathf.Sin(Time.time * 4) * 10,
                1);
            transform.localScale = vec;
        }
    }
}