namespace Hoang.CoreTech.Integration.Scoring
{
    public interface ILeaderBoardWriter : ILeaderBoardReader
    {
        /// <summary>
        /// Loads the leader board from the cloud or persistent storage
        /// </summary>
        void Initialise();

        /// <summary>
        /// Increments the current score by an amount
        /// </summary>
        /// <param name="amount">The amount to increase</param>
        void IncreaseCurrentScore(int amount);

        /// <summary>
        /// Commits the current score. Will replace high score and persist if higher.
        /// </summary>
        void CommitScores();
    }
}