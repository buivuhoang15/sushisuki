# SushiSuki

This is the Unity project for SushiSuki.

## Releasing

Refer to [this for updated documentation](https://github.com/conventional-changelog/standard-version)

Here is a paraphrased version:

_To cut a normal release:_

```sh
npm run release
```

_To do a prerelease (for instance 1.0.1-0):_
	
```sh
npm run release -- --prerelease
```

_To add a prefix to a prerelease (1.0.1-alpha.0):_

```sh
npm run release -- --prerelease alpha
```

_To do a manual versioned release (hotfixes for instance):_

```sh
#0.0.1:
npm run release -- --release-as patch
#0.1.0:
npm run release -- --release-as minor
#1.0.0:
npm run release -- --release-as major
```

_To overwrite the version completely:_

```sh
npm run release -- --release-as 1.1.0
```