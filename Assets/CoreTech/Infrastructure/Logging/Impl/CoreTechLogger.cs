using System;
using System.Collections.Generic;
using System.Text;

namespace Hoang.CoreTech.Infrastructure.Logging.Impl
{
    internal class CoreTechLogger : ICoreTechLogger
    {
        private readonly List<ILogTarget> _logTargets = new List<ILogTarget>();

        private const string Colon = ": ";

        private enum Severity
        {
            Debug,
            Info,
            Warn,
            Error
        }


        public void AddLogTarget(ILogTarget target)
        {
            if (target == null)
            {
                return;
            }

            _logTargets.Add(target);
        }

        public void Debug(object source, string message, params object[] optionalParams)
        {
            Log(Severity.Debug, source, message, optionalParams);
        }

        public void Info(object source, string message, params object[] optionalParams)
        {
            Log(Severity.Info, source, message, optionalParams);
        }

        public void Warn(object source, string message, params object[] optionalParams)
        {
            Log(Severity.Warn, source, message, optionalParams);
        }

        public void Error(object source, string message, params object[] optionalParams)
        {
            Log(Severity.Error, source, message, optionalParams);
        }

        public bool IsDebug
        {
            get
            {
#if DEBUG
                return true;
#else
                return false;
#endif
            }
        }

        private void Log(Severity severity, object source, string message, params object[] optionalParams)
        {
            var formattedMessage = string.Format(message, optionalParams);
            var stringBuilder = new StringBuilder();
            stringBuilder.Append(source.GetType().FullName);
            stringBuilder.Append(Colon);
            stringBuilder.Append(formattedMessage);

            var fullMessage = stringBuilder.ToString();

            foreach (var logTarget in _logTargets)
            {
                switch (severity)
                {
                    case Severity.Debug:
                        logTarget.Debug(fullMessage);
                        break;
                    case Severity.Info:
                        logTarget.Info(fullMessage);
                        break;
                    case Severity.Warn:
                        logTarget.Warn(fullMessage);
                        break;
                    case Severity.Error:
                        logTarget.Error(fullMessage);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(severity), severity, null);
                }
            }
        }
    }
}