using System;
using System.Collections;
using Hoang.CoreTech.Infrastructure.Async;
using Hoang.CoreTech.Integration.Unity;
using UnityEngine;

namespace Hoang.CoreTech.Integration.Async
{
    public class CoroutineDeferredCall : IDeferredCall
    {
        private readonly IUnityLifecycle _unity;
        private readonly float _delay;
        private readonly Action _callback;
        private bool _stopped;

        public CoroutineDeferredCall(IUnityLifecycle unity, TimeSpan delay, Action callback)
        {
            _unity = unity;
            _delay = (float) delay.TotalSeconds;
            _callback = callback;
        }

        public void Start()
        {
            _unity.StartUnityCoroutine(DoWork());
        }

        private IEnumerator DoWork()
        {
            if (_delay > 0 && !_stopped)
            {
                yield return new WaitForSecondsRealtime(_delay);
            }

            if (_stopped)
            {
                OnQuit?.Invoke();
                yield break;
            }

            _callback();
            OnQuit?.Invoke();
        }

        public void Stop()
        {
            _stopped = true;
        }

        public void Restart()
        {
            Stop();
            Start();
        }

        public event Action OnQuit;

        public void Dispose()
        {
            Stop();
        }
    }
}