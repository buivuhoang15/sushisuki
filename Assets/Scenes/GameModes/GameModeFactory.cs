using System;
using Hoang.CoreTech.Infrastructure.Logging;
using Hoang.CoreTech.Integration.GameSession;
using Hoang.CoreTech.Integration.Scoring;
using Hoang.CoreTech.Integration.Unity;
using Scenes.GameModes.Modes;
using Scenes.GameStart;

namespace Scenes.GameModes
{
    public class GameModeFactory: IGameModeFactory
    {
        private readonly IUnityLifecycle _unityLifecycle;
        private readonly IGameSessionWriter _gameSession;
        private readonly ILeaderBoardWriter _leaderBoard;
        private readonly ICoreTechLogger _logger;
        private readonly IRandomFoodGenerator _randomFoodGenerator;

        public GameModeFactory(IUnityLifecycle unityLifecycle,
            IGameSessionWriter gameSession,
            ILeaderBoardWriter leaderBoard,
            ICoreTechLogger logger,
            IRandomFoodGenerator randomFoodGenerator)
        {
            _unityLifecycle = unityLifecycle;
            _gameSession = gameSession;
            _leaderBoard = leaderBoard;
            _logger = logger;
            _randomFoodGenerator = randomFoodGenerator;
        }

        public IGameMode Create(GameMode mode)
        {
            switch (mode)
            {
                case GameMode.FullCourse:
                    return new FullCourse(_gameSession, _leaderBoard, _logger);
                case GameMode.QuickMeal:
                    return new QuickMeal(_gameSession, _leaderBoard, _unityLifecycle, _logger);
                case GameMode.PickyEater:
                    return new PickyEater(_gameSession, _leaderBoard, _unityLifecycle, _logger, _randomFoodGenerator);
                case GameMode.Relax:
                    return new Relax(_gameSession, _leaderBoard, _logger);
                default:
                    throw new GameModeNotSupportedException(nameof(mode), mode, null);
            }
        }
    }

    public class GameModeNotSupportedException : ArgumentOutOfRangeException
    {
        public GameModeNotSupportedException(string modeName, GameMode mode, string message)
        : base(modeName, mode, message)
        {
        }
    }
}