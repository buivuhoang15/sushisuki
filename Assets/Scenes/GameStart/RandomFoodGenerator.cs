using System;

namespace Scenes.GameStart
{
    public class RandomFoodGenerator : IRandomFoodGenerator
    {
        private readonly int _numOfFoodSprites;
        private readonly Random _rand = new Random();

        public RandomFoodGenerator(int numOfFoodSprites)
        {
            _numOfFoodSprites = numOfFoodSprites;
        }
        
        public IFood Next()
        {
            var randomIndex = _rand.Next(0, _numOfFoodSprites - 1);
            return new Food {Type = randomIndex};
        }
        
        private class Food : IFood
        {
            public int Type { get; set; }
        }
    }
}