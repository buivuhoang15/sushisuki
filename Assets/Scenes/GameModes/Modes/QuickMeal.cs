using System;
using Hoang.CoreTech.Infrastructure.Logging;
using Hoang.CoreTech.Integration.GameSession;
using Hoang.CoreTech.Integration.Scoring;
using Hoang.CoreTech.Integration.Unity;
using Scenes.GameStart;

namespace Scenes.GameModes.Modes
{
    public class QuickMeal: IGameMode
    {
        private readonly IGameSessionWriter _gameSession;
        private readonly ILeaderBoardWriter _leaderBoard;
        private readonly IUnityLifecycle _unityLifecycle;
        private readonly ICoreTechLogger _logger;
        private float _deltaTime;

        public IFood EdibleFood => null;
        public bool IsTimed => true;
        public TimeSpan RemainingTime { get; private set; } = TimeSpan.FromSeconds(31);
        public float SpeedMultiplier { get; private set; } = 1;

        public void FoodEscaped(IFood food)
        {
            _gameSession.GameOver();
        }

        public void FoodEaten(IFood food)
        {
            _leaderBoard.IncreaseCurrentScore(1);
        }

        public event Action<IFood> OnEdibleFoodChanged;

        public QuickMeal(IGameSessionWriter gameSession,
            ILeaderBoardWriter leaderBoard,
            IUnityLifecycle unityLifecycle,
            ICoreTechLogger logger)
        {
            _gameSession = gameSession;
            _leaderBoard = leaderBoard;
            _unityLifecycle = unityLifecycle;
            _logger = logger;

            _unityLifecycle.OnUpdate += Update;
        }

        private void Update(float deltaTime)
        {
            if (RemainingTime <= TimeSpan.Zero)
            {
                _logger.Debug(this, "Time ran out!");
                _gameSession.GameOver();
                return;
            }

            _deltaTime += deltaTime;
            RemainingTime = RemainingTime.Subtract(TimeSpan.FromSeconds(deltaTime));

            var speedIncreaseInterval = 5;
            if (_deltaTime > speedIncreaseInterval)
            {
                _deltaTime -= speedIncreaseInterval;
                SpeedMultiplier *= 2;
                _logger.Debug(this, "SpeedMultiplier: {0}", SpeedMultiplier);
            }
        }

        public void Dispose()
        {
            _unityLifecycle.OnUpdate -= Update;
        }
    }
}