using System;

namespace Hoang.CoreTech.Infrastructure.Async
{
    public interface IDeferredCallFactory : IDisposable
    {
        IDeferredCall Execute(TimeSpan delay, Action callback);
    }
}