﻿using Hoang.CoreTech.Integration.GameSession;
using Hoang.CoreTech.Integration.Scoring;
using UnityEngine.UI;

namespace Scenes.MainMenu
{
    public class MainMenu : SceneBootstrapper
    {
        public Text ScoreText;

        private IGameSessionWriter GameSession => Context.GameSession;
        private ILeaderBoardReader LeaderBoard => Context.LeaderBoard;

        protected override void OnStart()
        {
            base.OnStart();
            OnLeaderBoardUpdated();
            LeaderBoard.LeaderBoardUpdated += OnLeaderBoardUpdated;
        }

        private void OnLeaderBoardUpdated()
        {
            ScoreText.text = LeaderBoard.HighScore.ToString();
        }

        public void OnStartButtonClicked()
        {
            GameSession.SelectGameMode();
        }

        public void OnAdButtonClicked()
        {
            
        }

        protected override void OnDestroyed()
        {
            LeaderBoard.LeaderBoardUpdated -= OnLeaderBoardUpdated;
        }
    }
}