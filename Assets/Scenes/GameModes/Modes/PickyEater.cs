using System;
using Hoang.CoreTech.Infrastructure.Logging;
using Hoang.CoreTech.Integration.GameSession;
using Hoang.CoreTech.Integration.Scoring;
using Hoang.CoreTech.Integration.Unity;
using Scenes.GameStart;

namespace Scenes.GameModes.Modes
{
    public class PickyEater: IGameMode
    {
        private readonly IGameSessionWriter _gameSession;
        private readonly ILeaderBoardWriter _leaderBoard;
        private readonly IUnityLifecycle _unityLifecycle;
        private readonly ICoreTechLogger _logger;
        private readonly IRandomFoodGenerator _randomFoodGenerator;
        private IFood _edibleFood;

        public IFood EdibleFood
        {
            get { return _edibleFood; }
            private set
            {
                if (value == null || value.TheSameAs(_edibleFood))
                {
                    return;
                }

                _edibleFood = value;
                OnEdibleFoodChanged?.Invoke(value);
            }
        }

        public bool IsTimed => false;
        public TimeSpan RemainingTime => TimeSpan.Zero;
        public float SpeedMultiplier { get; } = 1;

        public PickyEater(IGameSessionWriter gameSession,
            ILeaderBoardWriter leaderBoard,
            IUnityLifecycle unityLifecycle,
            ICoreTechLogger logger,
            IRandomFoodGenerator randomFoodGenerator)
        {
            _gameSession = gameSession;
            _leaderBoard = leaderBoard;
            _unityLifecycle = unityLifecycle;
            _logger = logger;
            _randomFoodGenerator = randomFoodGenerator;
            _unityLifecycle.OnUpdate += Update;

            EdibleFood = _randomFoodGenerator.Next();
        }

        private void Update(float deltaTime)
        {
            
        }

        public void FoodEscaped(IFood food)
        {
            if (EdibleFood == null)
            {
                _logger.Error(this, "EdibleFood is not set up properly");
            }
            if (food.TheSameAs(EdibleFood))
            {
                _gameSession.GameOver();
            }
        }

        public void FoodEaten(IFood food)
        {
            if (EdibleFood == null)
            {
                _logger.Error(this, "EdibleFood is not set up properly");
            }
            if (!food.TheSameAs(EdibleFood))
            {
                _gameSession.GameOver();
                return;
            }

            _leaderBoard.IncreaseCurrentScore(1);
        }

        public event Action<IFood> OnEdibleFoodChanged;

        public void Dispose()
        {
            _unityLifecycle.OnUpdate -= Update;
        }
    }
}