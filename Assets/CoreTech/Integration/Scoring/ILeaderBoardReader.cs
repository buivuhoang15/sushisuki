using System;

namespace Hoang.CoreTech.Integration.Scoring
{
    public interface ILeaderBoardReader
    {
        /// <summary>
        /// The highest score the player has ever achieved
        /// </summary>
        int HighScore { get; }
        
        /// <summary>
        /// The current score the player has in the current session
        /// </summary>
        int CurrentScore { get; }

        /// <summary>
        /// Occurs when the current score has been updated
        /// </summary>
        event Action CurrentScoreUpdated;

        /// <summary>
        /// Occurs when the in memory leader board has been updated
        /// </summary>
        event Action LeaderBoardUpdated;
    }
}