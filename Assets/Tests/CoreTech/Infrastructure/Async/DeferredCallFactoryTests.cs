using System;
using System.Threading;
using Hoang.CoreTech.Infrastructure.Async;
using NUnit.Framework;

namespace Hoang.Tests.CoreTech.Infrastructure.Async
{
    public class DeferredCallFactoryTests
    {
        private IDeferredCallFactory _deferredCallFactory;

        [SetUp]
        public void SetUp()
        {
            _deferredCallFactory = new DeferredCallFactory();
        }

        [Test]
        public void ShouldDisposeAllScheduledCalls_OnDispose()
        {
            var wasCalled1 = false;
            var wasCalled2 = false;
            var finishEvent1 = new ManualResetEventSlim();
            var finishEvent2 = new ManualResetEventSlim();
            _deferredCallFactory.Execute(TimeSpan.FromMilliseconds(10), () =>
            {
                wasCalled1 = true;
                finishEvent1.Set();
            });
            _deferredCallFactory.Execute(TimeSpan.FromMilliseconds(10), () =>
            {
                wasCalled2 = true;
                finishEvent2.Set();
            });

            _deferredCallFactory.Dispose();

            finishEvent1.Wait(TimeSpan.FromMilliseconds(20));
            finishEvent2.Wait(TimeSpan.FromMilliseconds(20));
            Assert.IsFalse(wasCalled1);
            Assert.IsFalse(wasCalled2);
        }
    }
}