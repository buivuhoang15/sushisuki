using Hoang.CoreTech;

namespace Scenes
{
    public class SceneBootstrapper: CoreTechMonoBehaviour
    {
        protected override void OnStart()
        {
            SceneMapper.Initialise();
        }
    }
}