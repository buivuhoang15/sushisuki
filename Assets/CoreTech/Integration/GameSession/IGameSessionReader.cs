using System;

namespace Hoang.CoreTech.Integration.GameSession
{
    public enum GameState
    {
        GameOver,
        SelectGameMode,
        GameStart
    }
    
    public interface IGameSessionReader
    {
        GameState State
        {
            get;
#if DEBUG
            set;
#endif
        }
        
        /// <summary>
        /// Occurs when the player presses start game
        /// </summary>
        event Action OnStartGame;

        /// <summary>
        /// Occurs when the player enters the game mode selection
        /// </summary>
        event Action OnSelectGameMode;
        
        /// <summary>
        /// Occurs when the player lose
        /// </summary>
        event Action OnGameOver;
    }
}