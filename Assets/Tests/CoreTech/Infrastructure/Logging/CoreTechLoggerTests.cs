using Hoang.CoreTech.Infrastructure.Logging;
using Hoang.CoreTech.Infrastructure.Logging.Impl;
using NSubstitute;
using NUnit.Framework;

namespace Hoang.Tests.CoreTech.Infrastructure.Logging
{
    public class CoreTechLoggerTests
    {
        private ICoreTechLogger _logger;
        private ILogTarget _target;

        [SetUp]
        public void SetUp()
        {
            _logger = new CoreTechLogger();
            _target = Substitute.For<ILogTarget>();
            _logger.AddLogTarget(_target);
        }

        [Test]
        public void ShouldDelegateToLogTargets_OnLog()
        {
            var target2 = Substitute.For<ILogTarget>();
            _logger.AddLogTarget(target2);
            
            _logger.Debug(this, "Test");

            _target.Received().Debug(Arg.Any<string>());
            target2.Received().Debug(Arg.Any<string>());
        }

        [Test]
        public void ShouldConstructCorrectLogMessage()
        {
            _logger.Debug(this, "Test");

            var expectedMessage = $"{GetType().FullName}: Test";
            _target.Received().Debug(expectedMessage);
        }

        [Test]
        public void ShouldCallCorrectLogMethod_OnLog()
        {
            _logger.Debug(this, "Test");
            _target.Received().Debug(Arg.Any<string>());
            
            _logger.Info(this, "Test");
            _target.Received().Info(Arg.Any<string>());
            
            _logger.Warn(this, "Test");
            _target.Received().Warn(Arg.Any<string>());
            
            _logger.Error(this, "Test");
            _target.Received().Error(Arg.Any<string>());
        }
    }
}